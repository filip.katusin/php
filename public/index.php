<?php

use DI\Container;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;

require __DIR__ . '/../vendor/autoload.php';

// Create Container using PHP-DI
$container = new Container();

// Konfigurácia aplikácie
$container->set('x', function () {
    return 5;
});

// Konfigurácia aplikácie
$container->set('config', function () {
    return require __DIR__ . '/../application/config.php';
});

// Pripojenie na databázu
$container->set('dbDriver', function ($container) {
    $cfg = $container->get('config');
    $driver = new \PDO("mysql:host=" . $cfg['db.host'] . ";dbname=" . $cfg['db.name'] . ";port=" . $cfg['db.port'], $cfg['db.user'], $cfg['db.passwd']);
    $driver->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    return $driver;
});

// Set container to create App with on AppFactory
AppFactory::setContainer($container);
$app = AppFactory::create();

$app->addRoutingMiddleware();

// Add Error Middleware
$errorMiddleware = $app->addErrorMiddleware(true, true, true);

// Get the default error handler and register my custom error renderer.
$errorHandler = $errorMiddleware->getDefaultErrorHandler();
$errorHandler->forceContentType('application/json');

$app->get('/user', function (Request $request, Response $response, array $args) {
    $return = new stdClass();
    $return->users = [];

    if ($this->has('dbDriver')) {
        $dbDriver = $this->get('dbDriver');

        $cond = [];
        $bind = [];

        $queryParams = $request->getQueryParams();
        foreach ($queryParams as $param => $value) {
            if ($value) {
                switch ($param) {
                    case 'name':
                        $cond[] = 'name LIKE :name';
                        $bind[':name'] = $value . '%';
                        break;
                    case 'surname':
                        $cond[] = 'surname LIKE :surname';
                        $bind[':surname'] = $value . '%';
                        break;
                }
            }
        }

        $query = "SELECT * FROM user";
        if ($cond) {
            $query .= " WHERE " . implode(' AND ', $cond);
        }

        $stmt = $dbDriver->prepare($query);
        $stmt->execute($bind);

        $dbUsers = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach ($dbUsers as $dbUser) {
            $user = new stdClass();
            $user->id = $dbUser['id'];
            $user->name = $dbUser['name'];
            $user->surname = $dbUser['surname'];

            $return->users[$user->id] = $user;
        }
    }

    $payload = json_encode($return, JSON_PRETTY_PRINT);
    $response->getBody()->write($payload);
    return $response->withHeader('Content-Type', 'application/json');
});

$app->get('/user/{id}', function (Request $request, Response $response, array $args) {
    if ($this->has('dbDriver')) {
        $dbDriver = $this->get('dbDriver');

        $stmt = $dbDriver->prepare("SELECT * FROM user WHERE id = :id");
        $stmt->execute([
            ':id' => (int) $args['id']
        ]);

        $user = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($user) {
            $payload = json_encode($user, JSON_PRETTY_PRINT);
            $response->getBody()->write($payload);
            return $response->withHeader('Content-Type', 'application/json');
        }
    }

    throw new \Slim\Exception\HttpNotFoundException($request);
});

$app->get('/', function (Request $request, Response $response, array $args) {
    $payload = json_encode([
        'hello' => $request->getQueryParams()['name'] ?? 'world'
    ], JSON_PRETTY_PRINT);
    $response->getBody()->write($payload);
    return $response->withHeader('Content-Type', 'application/json');
});

$app->run();