# Základy

Pre potreby vytvorenia Rest API klienta v PHP nám poslúži PHP Slim framework. 

- Framework pre API: https://www.slimframework.com/ - Docs: https://www.slimframework.com/docs/v4/
- Response format: `JSON`
- Data storage: `Mariadb` (https://mariadb.org/) - PHP driver (https://www.php.net/manual/en/ref.pdo-mysql.php)
- Request authorization: `Bearer token`
- PHP Composer: https://getcomposer.org/

# Rest API

Na začiatok si vytvoríme jednoduchú rest API službu, ktorá bude obsahovať endpointy pre:

- zoznam používateľov
- detail používateľa
- vytvorenie používateľa
- zmazanie používateľa

Dáta o používateľoch sa budú ukladať do Mariadb databázy. Pre tieto potreby je nutné mať vytvorený DB konektor na mariadb databázu cez PDO driver. DB konektor bude PHP trieda, ktorá vytvorí spojenie s databázou a umožní nám vykonávanie jednotlivých databázových dopytov prostredníctvom PDO drivera.

Následne si to skomplikujeme a rest API službu zabezpečíme Auth Bearer tokenom.

# Projekt

Inicializácia projektu spočíva v nainštalovaní Mariadb databázy a nainštalovaní dát cez PHP composer príkazom

```
composer install
```

V roote projektu je dump databázy

Spustenie api cez terminál

```
cd public
php -S localhost:8888
```